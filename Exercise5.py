import pprint
import scipy
import scipy.linalg   # SciPy Linear Algebra Library
import numpy as np
import math
from numpy import dot, array

#----------- Exercise 5 - Part a -------------- 

A = [[7/2,sqrt(3)/2],[sqrt(3)/2,5/2]]

detA = numpy.linalg.det(A)
print('Determinant of matrix A is:')
print(detA)

trA = np.trace(A)
print('Trace of matrix A is:')
print(trA)

e_valsA, e_vecsA = LA.eig(A)
print('Eingevalues of matrix A is:')
print(e_valsA)

#----------- Exercise 5 - Part b -------------- 
Q = [
     [(math.sqrt(6)) + ((math.sqrt(2))/4) , (-(math.sqrt(6))) + ((math.sqrt(2))/4)],
     [(math.sqrt(6)) - ((math.sqrt(2))/4) , ((math.sqrt(6))) + ((math.sqrt(2))/4)]
    ]

matrix = np.dot(A,Q)
transposteQ = list(zip(*Q))

A1 = np.dot( (np.dot(Q,A)),(list(zip(*Q))) )

print('The value of A1, given by A1 = Q*A*QT, is:')
print(A1)

detA1 = numpy.linalg.det(A1)
print('Determinant of matrix A1 is:')
print(detA1)

trA1 = np.trace(A1)
print('Trace of matrix A1 is:')
print(trA1)

e_valsA1, e_vecsA1 = LA.eig(A1)
print('Eingevalues of matrix A1 is:')
print(e_valsA1)

#----------- Exercise 5 - Part c -------------- 
#Q with alfa = pgreco/3
Q2 = [[1/2,-(sqrt(3)/2)],[sqrt(3)/2,1/2]]

A2 = np.dot( (np.dot(Q2,A)),(list(zip(*Q2))) )
print('A1 calculate with alfa in Q = pgreco/3')
print(A2)
