import pprint
import scipy
import scipy.linalg   # SciPy Linear Algebra Library
from numpy import dot, array

#----------- Exercise 3 - Part a -------------- 

A = [[2,3],[6,13]]
P, L, U = scipy.linalg.lu(A)

print ("A:")
print(A)

print ("P:")
print(P)

print ("L:")
print(L)

print ("U:")
print(U)


inverseA = numpy.linalg.inv(A)
print("inverse of matrix A")
print(inverseA)

b = [1,2]
c = np.dot(inverseA,b) 
print('inverseA*b')
print(c)

#----------- Part b -------------- 

# The 'gauss' function takes two matrices, 'a' and 'b', with 'a' square, 
# and it return the determinant  of 'a' and a matrix 'x' such that a*x = b.
# If 'b' is the identity, then 'x' is the inverse of 'a'.

def linearsolver(A,b):
  n = len(A)
  M = A

  i = 0
  for x in M:
   x.append(b[i])
   i += 1

  for k in range(n):
   for i in range(k,n):
     if abs(M[i][k]) > abs(M[k][k]):
        M[k], M[i] = M[i],M[k]
     else:
        pass

   for j in range(k+1,n):
       q = float(M[j][k]) / M[k][k]
       for m in range(k, n+1):
          M[j][m] -=  q * M[k][m]

  x = [0 for i in range(n)]

  x[n-1] =float(M[n-1][n])/M[n-1][n-1]
  for i in range (n-1,-1,-1):
    z = 0
    for j in range(i+1,n):
        z = z  + float(M[i][j])*x[j]
    x[i] = float(M[i][n] - z)/M[i][i]

  print("Solution x using Gauss of vector x")
  print(x)
 
b = [1,2]

linearsolver(A,b)

#----------- Part c -------------- 
# The exercise it wasn't too complex, it is useful for a general review, 
# especially if you don't do review linear algebra for a while.
# The fact of not being able to implement all without using the library takes a lot of time 
# but it forces to review all the procedures, helping in the review.
