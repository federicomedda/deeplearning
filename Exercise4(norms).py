#function for calculate the norma

#norma1
def norma1( v ):
   
   value = 0
   	n = len(v)
   	if (n < 1) : print('Error')
   else :
   	for item in v:
   		if(item < 0) : 
   			value = (-(item)) + value
   		else : value = item + value

   return value

#calculate root n
   def anysqrt(base,n):

    return pow(n,1/base)

#norma2
def norma2( v ):
   
   value = 0
   	n = len(v)
   	if (n < 1) : print('Error')
   else :
   	for item in v:
   		value = anysqrt((item**2),2) + value

   return value

#norma8
   def norma8( v ):

   	value = 0

   		n = len(v)
   	if (n < 1) : print('Error')
   else :
   	for item in v:
   		if(item < 0) : 
   			value = (-(item)**8) + value
   		else : value = (item**8) + value

   		value = anysqrt(value,8)

   	return value

#infinite norma
def normaInfinite( v ):
   
   value = 0
   	n = len(v)
   	if (n < 1) : print('Error')
   else :
   	for item in v:
   		if(item > value) : 
   			value = item
   		else : value = value

   return value
