from numpy import *   # Import NumPy package which enables all the fun with algebra

#----------- Exercise 2 with numpy - Part a -------------- 
A = (1/4)*np.array([[7, -(np.sqrt(3))],[-(np.sqrt(3)),5]])
λ, Q = linalg.eig(A)
print('Eigenvalues:')
print(λ)
print('\nEigenvectors:')
print(Q)

#----------- Wwithout numpy - Part a -------------- 
A = (1/4)*np.array([[7, -(np.sqrt(3))],[-(np.sqrt(3)),5]])
vlamda = 1

def eingenvalues( A ):
   lamda = -(A[0,0])-(A[1,1])
   lamda2 = 1
   TNoto = -(A[1,0] * A[0,1]) + (A[0,0])*(A[1,1]) 

   v1 = (-(lamda)+ np.sqrt((lamda)*(lamda)-(4*lamda2*TNoto)))/ (2*lamda2)
   v2 = (-(lamda)- np.sqrt((lamda)*(lamda)-(4*lamda2*TNoto)))/ (2*lamda2)

   return v1, v2

λ1, λ2 = eingenvalues(A)

print("lamda1 is equal to: ")
print(λ1)
print("lamda2 is equal to: ")
print(λ2)


def eingenvectors( A, λ ):

    x = ((-(A[0,1]))/(A[0,0] - λ)) 
    y = ((-(A[1,0]))/(A[1,1] - λ))
    
    return x, y

a1, a2 = eingenvectors(A, λ1)
print("eingervectors with λ1 is: ")
print(a1)
print(a2)

a3, a4 = eingenvectors(A, λ2)
print("eingevectors with λ2 is: ")
print(a3)
print(a4)

Q = np.array([[a1,a2],[a3,a4]])
Λ = np.array([[λ1,0],[0,λ2]])

print('matrix Q')
print(Q)
print('matrix Λ')
print(Λ)

#----------- Part b -------------- 
#Check ifmatrix is orthogonal or not 
  
def isOrthogonal(a, m, n) : 
    if (m != n) : 
        return False
      
    trans = [[0 for x in range(n)]  
                for y in range(n)]  
                  
    # Find transpose 
    for i in range(0, n) : 
        for j in range(0, n) : 
            trans[i][j] = a[j][i] 
              
    prod = [[0 for x in range(n)] 
               for y in range(n)]  
                 
    # Find product of a[][]  
    # and its transpose 
    for i in range(0, n) : 
        for j in range(0, n) : 
      
            sum = 0
            for k in range(0, n) : 
          
                # Since we are multiplying  
                # with transpose of itself. 
                # We use 
                sum = sum + (a[i][k] * 
                             a[j][k]) 
      
            prod[i][j] = sum
  
    # Check if product is  
    # identity matrix 
    for i in range(0, n) : 
        for j in range(0, n) : 
  
            if (i != j and prod[i][j] != 0) : 
                return False
            if (i == j and prod[i][j] != 1) : 
                return False
  
    return True
  
# Driver Code 
if (isOrthogonal(Q, 2, 2)) : 
    print ("Yes") 
else : 
    print ("No") 

#----------- Part c -------------- 
#Show that the matrix Qλ-1Qt is the inverse of A

inverseλ =[ λ[0]**-1, λ[1]**-1 ] 
transposeQ = Q.transpose()
inverseA = numpy.linalg.inv(A)

factorization = Q * inverseλ * transposeU

if ( factorization == inverseA) : 
    print ("Yes, Q * inverseλ * trasposeQ is equal to inverseA") 
else :
    print ("No, Q * inverseλ * trasposeQ is not equal to inverseA") 

#----------- Part d -------------- 
# The exercise it wasn't too complex, it is useful for a general review, 
# especially if you don't do review linear algebra for a while.
# The fact of not being able to fully use the numpy library takes a lot of time 
# but it forces to review all the procedures, helping in the review.